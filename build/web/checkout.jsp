<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Beauto's Books</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css"/>
</head>
<body>
    <h1>Thanks for your order</h1>
    <p>Here is the information you entered.</p>
    <table>
      <tr>
        <th>Cover</th>        
        <th>Title</th>
        <th>Price</th>
        <th>Amount</th>
        <th>Quantity</th>
      </tr>
      
      <c:set var="total" value="${0.00}" />
      <c:forEach var="item" items="${cart.items}">
            <c:set var="totalT" value="${totalT + item.total}"/>

        <tr>
          <td class="cover"><img src="${item.product.cover}" alt="Book cover Image Error" height="150"></td>
          <td><c:out value='${item.product.description}'/></td>
          <td>${item.product.priceCurrencyFormat}</td>
          <td>${item.totalCurrencyFormat}</td>
          <td>${item.quantity}</td>
        </tr>
      </c:forEach>
        <tr>
          <td><b>Total</b></td>
          <td></td>
          <td></td>
          <td><fmt:setLocale value="en_US"/>
          <fmt:formatNumber value="${totalT}" type="currency" minFractionDigits="2" maxFractionDigits="2"/></td>
          <td></td>
        </tr>
    </table>
    
        <form action="" method="post">
            <input type="hidden" name="action" value="return">
            <input type="submit" value="Return">
        </form>
        
    <p>&copy; Copyright ${currentYear} Beauto's Books.</p>
</body>
</html>