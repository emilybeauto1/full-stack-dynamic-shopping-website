<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Beauto's Books</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css"/>
</head>
<body>
    <div>
        <h1><b>Book List</b></h1>
        ${sqlResult}
    </div>

    <p>For customer service, please send an email to ${custServEmail}.</p>

    <p>&copy; Copyright ${currentYear} Beauto's Books
        All rights reserved.</p>
</body>
</html>