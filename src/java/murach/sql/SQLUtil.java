package murach.sql;

import java.sql.*;
//Emily Beauto, 12/2/23
//Description: builds my html table from the SQL data

public class SQLUtil {

    public static String getHtmlTable(ResultSet results)
            throws SQLException {
        
        StringBuilder htmlTable = new StringBuilder();
        ResultSetMetaData metaData = results.getMetaData();
        int columnCount = metaData.getColumnCount();

        htmlTable.append("<table>");

        // add header row
        htmlTable.append("<tr>");
        for (int i = 2; i <= columnCount; i++) {
            htmlTable.append("<th>");
            htmlTable.append(metaData.getColumnName(i));
            htmlTable.append("</th>");
        }
        //add extra box in index.jsp table
        htmlTable.append("<th>");
        htmlTable.append("</th>");
        htmlTable.append("</tr>");
        
        // add all other rows
        int bookIndex = 1;
        while (results.next()) {
            htmlTable.append("<tr>");
            for (int i = 2; i <= columnCount; i++) {
                htmlTable.append("<td>");
                if (i == 2) {
                    String imageColumn = "<img src=\"" + results.getString(i) + "\" alt=\"Book cover Image Error\" height=\"150\">";
                    htmlTable.append(imageColumn);
                }
                else if (i == 4) {
                    String currencyColumn = "$" + results.getString(i);
                    htmlTable.append(currencyColumn);
                } else {
                    htmlTable.append(results.getString(i));
                }
                htmlTable.append("</td>");
            }
            String tableForm = "<td><form action=\"cart\" method=\"post\"> <input type=\"hidden\" name=\"productCode\"  value=\"" + bookIndex + "\"> <input type=\"submit\" value=\"Add To Cart\"> </form></td>";
            htmlTable.append(tableForm);
            htmlTable.append("</tr>");
            bookIndex++;
        }

        htmlTable.append("</table>");
        return htmlTable.toString();
    }
}