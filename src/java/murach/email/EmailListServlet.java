package murach.email;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import murach.business.Cart;

import murach.business.User;
import murach.data.UserDB;
//Emily Beauto, 12/2/23
//Description: handle transfer of control among forms with their values/actions
public class EmailListServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/index.jsp";
        
        // get current action
        String action = request.getParameter("action");
        if (action == null) {
            action = "join";  // default action
        }

        // perform action and set URL to appropriate page        
        if (action.equals("join")) {    
            url = "/register.jsp";    // the "register" page
        }
        else if (action.equals("home")) {
            HttpSession session = request.getSession();
            Cart cart = (Cart) session.getAttribute("cart");
            cart = new Cart();
            session.setAttribute("cart", cart);
            
            url = "/index.jsp";    // the "join" page
        } 
        else if (action.equals("signIn")) {
            // get parameters from the request
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            
            // store data in User object
            User user = new User( firstName, lastName, email, password);
            
            // validate the parameters
            String message;
            if (UserDB.passwordMatch(user.getEmail(), user.getPassword())) {
                message = "";
                url = "/thanks.jsp";
                UserDB.insert(user);
                user = UserDB.selectUser(user.getEmail());
            } else if (UserDB.emailExists(user.getEmail()) && !UserDB.passwordMatch(user.getEmail(), user.getPassword())) {
                message = "The password does not match. Please try again.";
                url = "/signIn.jsp";
            }
            else {      
                message = "";
                url = "/register.jsp";
            }
            
            request.setAttribute("user", user);
            request.setAttribute("message", message);
            
        } else if (action.equals("register")) {
         // get parameters from the request
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            
            // store data in User object
            User user = new User( firstName, lastName, email, password);
            
            // validate the parameters
            String message;
            if (UserDB.emailExists(user.getEmail())) {
                message = "This email address already exists.<br>" +
                          "Please enter another email address.";
                url = "/register.jsp";
            }
            else {
                message = "";
                url = "/thanks.jsp";
                UserDB.insert(user);
            }
            
            request.setAttribute("user", user);
            request.setAttribute("message", message);
        }
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
}