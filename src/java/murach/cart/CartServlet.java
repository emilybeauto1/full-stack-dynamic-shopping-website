package murach.cart;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import murach.data.*;
import murach.business.*;
//Emily Beauto, 12/1/23
//Description: controls my cart functions/mapping
public class CartServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext sc = getServletContext();
        
        // get current action
        String action = request.getParameter("action");
        if (action == null) {
            action = "cart";  // default action
        }
        
        // perform action and set URL to appropriate page
        String url = "/index.jsp";
        if (action.equals("shop")) {
            url = "/index.jsp";    // the "index" page
        } 
        else if (action.equals("return")) {
            HttpSession session = request.getSession();
            Cart cart = (Cart) session.getAttribute("cart");
            cart = new Cart();
            session.setAttribute("cart", cart);
            
            url = "/index.jsp";
        }
        else if (action.equals("cart")) {
            String productCode = request.getParameter("productCode");
            String quantityString = request.getParameter("quantity");
            System.out.println("quantity =  " + quantityString);
            
            HttpSession session = request.getSession();
            Cart cart = (Cart) session.getAttribute("cart");
            if (cart == null) {
                cart = new Cart();
            }

            //if the user enters a negative or invalid quantity,
            //the quantity is automatically reset to 1.
            int quantity;
            try {
                quantity = Integer.parseInt(quantityString);
                if (quantity < 0) {
                    quantity = 1;
                }
            } catch (NumberFormatException nfe) {
                quantity = 1;
            }
//database
            

            String path = sc.getRealPath("/WEB-INF/products.txt");
            System.out.println("productCode = " + productCode);
            Product product = ProductIO.getProduct(productCode, path);
           
            LineItem lineItem = new LineItem();
            lineItem.setProduct(product);
            lineItem.setQuantity(quantity);
            if (quantity > 0) {
                cart.addItem(lineItem);
            } else if (quantity == 0) {
                cart.removeItem(lineItem);
            }

            //cart.getCartTotal(lineItem);
            
            session.setAttribute("cart", cart);
            url = "/cart.jsp";
        }
        else if (action.equals("checkout")) {  
            url = "/signIn.jsp";
        }

        sc.getRequestDispatcher(url)
                .forward(request, response);
    }
}