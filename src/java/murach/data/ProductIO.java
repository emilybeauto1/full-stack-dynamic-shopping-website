package murach.data;

import java.io.*;
import java.sql.*;
import java.util.*;

import murach.business.*;
//Emily Beauto, 12/2/23
//Description: creates my products from sql database Book
public class ProductIO {

    public static Product getProduct(String code, String filepath) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "Select * from Book where BookID = " + code;
        try {
            Statement statement = connection.createStatement();

            ResultSet results = statement.executeQuery(query);
                
            results.next();
            String coverS = results.getString(2);
            String titleS = results.getString(3);
            double priceS = results.getDouble(4);   
            Product p = new Product();
            p.setCode(code);
            p.setCover(coverS);
            p.setDescription(titleS);
            p.setPrice(priceS);
            return p;
        
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static ArrayList<Product> getProducts(String filepath) {
        
        ArrayList<Product> products = new ArrayList<Product>();
        File file = new File(filepath);
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "Select * from Book";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            String codeS = resultSet.getString(1);
            String coverS = resultSet.getString(2);
            String titleS = resultSet.getString(3);
            double priceS = resultSet.getDouble(4);
            Product p = new Product();
            p.setCode(codeS);
            p.setCover(coverS);
            p.setDescription(titleS);
            p.setPrice(priceS);
            products.add(p);
            return products;
        
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
}