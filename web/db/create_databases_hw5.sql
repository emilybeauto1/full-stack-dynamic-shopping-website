/***********************************************************
* Create the database named hw5, its tables, and a database user
************************************************************/

DROP DATABASE IF EXISTS hw5;

CREATE DATABASE hw5;

USE hw5;

CREATE TABLE User (
  UserID INT NOT NULL AUTO_INCREMENT,
  Email VARCHAR(50),
  FirstName VARCHAR(50),
  LastName VARCHAR(50),
  Password VARCHAR(50),
  
  PRIMARY KEY(UserID) 
);

INSERT INTO User 
  (Email, FirstName, LastName, Password)
VALUES 
  ('bat@gmail.com', 'Bat', 'Man', 'bat'),
  ('spider@gmail.com', 'Spider', 'Man', 'spider'), 
  ('super@gmail.com', 'Super', 'Man', 'super');

 
 -- Create student and grant privileges


CREATE TABLE Book (
BookID INT NOT NULL AUTO_INCREMENT,
Cover NVARCHAR(210),
Title VARCHAR(50),
Price DOUBLE,
PRIMARY KEY(BookID)
);
INSERT INTO Book
(Cover, Title, Price)
VALUES
('https://m.media-amazon.com/images/I/41QZuBj-CPL._SY445_SX342_.jpg', 'Green Eggs and Ham', '7.96'),
('https://m.media-amazon.com/images/I/51QfV2q+3xL._SY445_SX342_.jpg', 'How The Grinch Stole Christmas', '11.99'),
('https://i5.walmartimages.com/seo/The-Nightmare-Before-Christmas-Walmart-Exclusive-9780593480410_425662a0-17b7-4bec-87da-484dcf587092.cdf7846896a4b5161e8672920eb9ed10.jpeg?odnHeight=612&odnWidth=612&odnBg=FFFFFF', 'The Nightmare Before Christmas', '8.98'),
('https://m.media-amazon.com/images/I/71FTb9X6wsL._AC_UF1000,1000_QL80_.jpg', 'The Great Gatsby', '6.89');  

SELECT * FROM Book;


DELIMITER //
CREATE PROCEDURE drop_user_if_exists()
BEGIN
    DECLARE userCount BIGINT DEFAULT 0 ;

    SELECT COUNT(*) INTO userCount FROM mysql.user
    WHERE User = 'student' and  Host = 'localhost';

    IF userCount > 0 THEN
        DROP USER student@localhost;
    END IF;
END ; //
DELIMITER ;

CALL drop_user_if_exists() ;

CREATE USER student@localhost IDENTIFIED BY 'sesame';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP
ON hw5.*
TO student@localhost;

  
USE hw5;