<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Beauto's Books</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css"/>
</head>
<body>
    <h1>Your cart</h1>
    <table>
      <tr>
        <th>Cover</th>        
        <th>Title</th>
        <th>Price</th>
        <th>Amount</th>
        <th>Quantity</th>
        <th></th>
      </tr>
      <c:forEach var="item" items="${cart.items}">
      <tr>
        <td class="cover"><img src="${item.product.cover}" alt="Book cover Image Error" height="150"></td>
        <td><c:out value='${item.product.description}'/></td>
        <td>${item.product.priceCurrencyFormat}</td>
        <td>${item.totalCurrencyFormat}</td>
        <td>
            <form action="" method="post">
                <input type="hidden" name="productCode" 
                       value="<c:out value='${item.product.code}'/>">
                <input type=text name="quantity" 
                       value="<c:out value='${item.quantity}'/>"  
                       id="quantity">
                <input type="submit" value="Update">
            </form>
        </td>   
        <td>
            <form action="" method="post">
                <input type="hidden" name="productCode" 
                       value="<c:out value='${item.product.code}'/>">
                <input type="hidden" name="quantity" value="0">
                <input type="submit" value="Remove Item">
            </form>
        </td>
      </tr>
      </c:forEach>
    </table>

    <p><b>To change the quantity</b>, enter the new quantity 
        and click on the Update button.</p>
    
    <form action="" method="post">
        <input type="hidden" name="action" value="shop">
        <input type="submit" value="Continue Shopping">
    </form>

    <form action="" method="post">
        <input type="hidden" name="action" value="checkout">
        <input type="submit" value="Checkout">
    </form>
    
    <p>&copy; Copyright ${currentYear} Beauto's Books.</p>

</body>
</html>