Used HTML, Java, JavaScript, SQL, and CSS to develop a dynamic 6-page web project, encompassing home, registration, sign-in, cart, checkout, and confirmation pages.
Utilized servlets to maintain website functionality, ensuring seamless user experience.
Implemented servlets, visual aesthetics, input validation, and database management.
